package org.propertiestest.reacttest.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    @Value("${message.first}")
    String test;
    @Value("${my.test}")
    String test2;

    @GetMapping("/")
    public String test(Model model){
        model.addAttribute("test",test);
        model.addAttribute("test2",test2);
        return "welcome";
    }
}
